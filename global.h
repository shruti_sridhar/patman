// PatMaN DNA pattern matcher
// (C) 2007 Kay Pruefer, Udo Stenzel
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at
// your option) any later version.  See the LICENSE file for details.

#ifndef INCLUDED_GLOBAL_H
#define INCLUDED_GLOBAL_H

#include <iosfwd>

extern size_t cutoff ;
extern size_t allow_gaps ;
extern long position_genome ;
extern const char* genome_name ;
extern int only_plus_strand ;
extern int discount_adenine ;
extern int quiet ;
extern int chop5 ;
extern std::ostream *output ;
extern int debug_flags ;
extern int do_prefetch ;
extern long long total_nodes ;

static const int debug_notquiet   = 1 ;
static const int debug_verbose    = 2 ;
static const int debug_trie       = 4 ;
static const int debug_countnodes = 8 ;
static const int debug_numnodes   = 16 ;
static const int debug_sequence   = 32 ;

#endif

