// PatMaN DNA pattern matcher
// (C) 2007 Kay Pruefer, Udo Stenzel
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at
// your option) any later version.  See the LICENSE file for details.

#include "fasta.h"

using namespace std ;

fasta* fasta_fac::get(  ) 
{
	string headline ;
	fasta* ret = new fasta ;
	ret->null = 0 ;
	// headline
	getline( in, ret->headerline ) ;
	line++ ;
	if ( ret->headerline[0] != '>' ) {
		if ( !in.eof() ) cerr << "No headerline in fasta file at line " << line << endl ;
		ret->null = 1 ;
		return ret ;
	} else {
		// cut the '>' in headerline
		ret->headerline = ret->headerline.substr( 1, ret->headerline.size()-1 ) ;

		// read the sequence
		// handles eof, \n and '>', everything else is
		// considered part of the sequence.
		int i ;
		while ( (i = in.get()) && in.good() ) {
			if ( i == '>' ) {
				in.putback( i ) ;
				return ret ;
			} else {
				// add to seq ;
				if ( i != '\n' ) 
					ret->seq += static_cast<char>(i) ;
				else 
					line++ ;
			}
		}
		return ret ;
	}
}
